
const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();




gulp.task('css', function () {
	return gulp.src('app/scss/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('app/css'));
		
});

gulp.task('clean', function (cd) {
	gulp.src('app/css/style.css')
		.pipe(cleanCSS())
		.pipe(rename({
			suffix: '.min'
		  }))
		.pipe(gulp.dest('dist/css'))
		cd()
});

gulp.task('watch', () => {
	gulp.watch('app/scss/*.scss', (done) => {
		gulp.series(['css'])(done);
    });
});

gulp.task('server', function () {
	browserSync.init({
		notify: false,
		port: 3002,
		server: {
			baseDir: 'app'
		}
	});

	gulp.watch('app/index.html').on("change", browserSync.reload);
	gulp.watch('app/css/style.css').on("change", browserSync.reload);
	gulp.watch('app/scss/style.scss').on("change", browserSync.reload);


});